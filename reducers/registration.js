import {ACTION_TYPES} from "../actions/registration";

const initialState = {
    list:[]
}

export const registration = (state=initialState,action) => {
    switch(action.type){
        case ACTION_TYPES.FETCH_ALL:
        return{
            ...state,
            list:[...action.payload]
        }

        default:
        return state;
    }
}