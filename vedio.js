import React from "react";
import {
  FlatList,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
} from "react-native";

export class vedio extends React.Component {
  static nagivationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = { listLoaded: false };
  }
  componentDidMount() {
    return fetch(
      "https://www.googleapis.com/youtube/v3/search?part=snippet&q=HealthKNOCKS&type=video&key=AIzaSyANkNf18p8Q0_xrRYElBfoADKkiI3HJXZk&maxResults=9"
    )
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          listLoaded: true,
          vedioList: Array.from(responseJson.items),
        });
      })
      .catch((error) => {
        console.log("Api call error");
        alert(error.message);
      });
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View>
        {this.state.listLoaded && (
          <View Style={{ paddingTop: 30 }}>
            <FlatList
              data={this.state.vedioList}
              renderItem={({ item }) => (
                <TubeItem
                  navigate={navigate}
                  id={item.id.videoId}
                  title={item.snippet.title}
                  imageSrc={item.snippet.thumbnails.high.url}
                />
              )}
            />
          </View>
        )}

        {!this.state.listLoaded && (
          <View style={{ paddingTop: 30 }}>
            <Text>LOADING</Text>
          </View>
        )}
      </View>
    );
  }
}
export class TubeItem extends React.Component {
  onPress = () => {
    // console.log(this.props.id);
    //alert("videoId=" + this.props.id + " Title=" + this.props.title);
    this.props.navigate("vedioDetailRT", { ytubeID: this.props.id });
  };
  render() {
    return (
      <TouchableWithoutFeedback onPress={this.onPress}>
        <View style={{ paddingTop: 20, alignItems: "center" }}>
          <Image
            style={{ width: "100%", height: 200 }}
            source={{ uri: this.props.imageSrc }}
          />
          <Text>{this.props.title}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}