import React, {useState, useEffect} from "react";
import { Button, View, StyleSheet, Text, TextInput } from "react-native";
import { connect } from "react-redux";
import * as actions from "../actions/registration"

const Registration = (props) => {
    
    useEffect(() => {
        props.fetchAllRegistration ()
    }, [])

    return (
        <View>
            <Text>
            Get API Data of Registered Users
            {/* <Text>{props.registrationList.map((record, index) => {
                return (<TableRow>
                    <TableCell>{record.firstName}</TableCell>
                </TableRow>)
            })}</Text> */}
            </Text>
            
    </View>
    );
};

const mapStateToProps = state => ({
    registrationList: state.registration.list
})


const mapActionToProps = {
    fetchAllRegistration:actions.fetchAll
}

const styles = StyleSheet.create({

});

export default connect(mapStateToProps, mapActionToProps)(Registration);