import React from "react";
import { Text, View } from "react-native";
import { WebView } from "react-native-webview";

export class vedioDetail extends React.Component {
  static navigationOptions = { header: null };

  render() {
    let tubeId = this.props.navigation.getParam("ytubeID", "NO VIDEO");
    let tubeUrl = "https://www.youtube.com/embed/";
    return (
      <WebView
        style={{ marginTop: 20 }}
        javaScriptEnabled={true}
        source={{ uri: tubeUrl + tubeId }}
      />
    );
  }
}