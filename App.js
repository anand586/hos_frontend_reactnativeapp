
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  FlatList,
  ActivityIndicator,
  Platform
} from "react-native";
import { render } from "react-dom";
import axios from "axios";
import { store } from "./actions/store";
import { Provider } from "react-redux";
import Registration from "./components/Registration";

function App() {

  return (
    <Provider store={store}>
      <Registration />
    </Provider>

    // <View style={{ padding: 50 }}>
    //   <View style={{}}>
    //     <Text style={{ color: "green", fontSize: 40 }}>Registration</Text>
    //   </View>
    //   <View style={styles.textDiv}>
    //     <TextInput placeholder="first name" style={styles.textInputStyle} />
    //     <TextInput placeholder="last name" style={styles.textInputStyle} />
    //     <TextInput placeholder="user name" style={styles.textInputStyle} />
    //     <TextInput placeholder="password" style={styles.textInputStyle} />
    //     <TextInput
    //       placeholder="confirm password"
    //       style={styles.textInputStyle}
    //     />
    //     <TextInput placeholder="email" style={styles.textInputStyle} />
    //   </View>
    //   <View>
    //     <Button title="SAVE" />
    //   </View>
    // </View>
  );

}

const styles = StyleSheet.create({
  textInputStyle: {
    borderColor: "black",
    borderWidth: 2,
    fontSize: 20,
    paddingStart: 2,
    width: "100%",
    height: "13%",
  },
  textDiv: {
    padding: 4,
    paddingStart: 1,
    flexDirection: "column",
    justifyContent: "space-evenly",
    alignContent: "center",
    width: 270,
    height: 400,
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});


export default App;